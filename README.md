# Recipe App API Proxy

NGINX proxy app

## Usage

### Environment Variables

* 'LISTEN_PORT' - Port to listen on (default: 8000)

* 'APP_HOST' - Hostname of the app to forward requests to (default: app)

* 'APP_PORT' - Port of the app to forward requests to (default: 9000)


- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
